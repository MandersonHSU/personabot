package com.porsonabot.personabot;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class setUp3Activity extends AppCompatActivity {
    Button btnSetDone;


    public void pressDone(){
        btnSetDone = findViewById(R.id.btnDone);
        btnSetDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent set_done = new Intent(setUp3Activity.this,SetUp2Activity.class);
                startActivity(set_done);
            }
        });
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_up3);
        pressDone();

    }
}
