package com.porsonabot.personabot;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class SetUp2Activity extends AppCompatActivity {
    Button btnSet3;


    public void pressNext(){
        btnSet3 = findViewById(R.id.btnSet3);
        btnSet3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent set_Up = new Intent(SetUp2Activity.this,setUp3Activity.class);
                startActivity(set_Up);
            }
        });
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_up2);
        pressNext();

    }
}