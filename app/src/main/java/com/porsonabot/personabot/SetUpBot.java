package com.porsonabot.personabot;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class SetUpBot extends AppCompatActivity {
    Button btnSet2;


    public void pressSet(){
        btnSet2 = findViewById(R.id.btnSetUp2);
        btnSet2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent setUp = new Intent(SetUpBot.this,SetUp2Activity.class);
                startActivity(setUp);
            }
        });
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_up_bot);
        pressSet();

    }
}
