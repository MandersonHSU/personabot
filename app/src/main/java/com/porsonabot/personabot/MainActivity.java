package com.porsonabot.personabot;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.view.View;
import android.content.Intent;

public class MainActivity extends AppCompatActivity {

    Button btnName;

    public void pressStart(){
        btnName = findViewById(R.id.btnStart);
        btnName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent setupBot = new Intent(MainActivity.this,SetUpBot.class);
                startActivity(setupBot);
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pressStart();

    }
}
